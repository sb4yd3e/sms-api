import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sms_inbox/flutter_sms_inbox.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SMS Auto',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Read SMS API'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final SmsQuery _query = SmsQuery();
  late Timer _timer;
  var database;
  RegExp regExpOtp = RegExp(
    r"<OTP \d+>",
    caseSensitive: true,
    multiLine: false,
  );
  RegExp regExpRef = RegExp(
    r"<Ref. \w+>",
    caseSensitive: true,
    multiLine: false,
  );

  Future<void> startTimer() async {
    final path = join(getDatabasesPath().toString(), 'otp_database.db');
    await deleteDatabase(path);

    database = openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE otp(id INTEGER PRIMARY KEY, ref VARCHAR(6), otp VARCHAR(6), message VARCHAR(255))');
        });

    if (await Permission.sms
        .request()
        .isGranted) {
      const oneSec = Duration(seconds: 10);
      _timer = Timer.periodic(oneSec, (Timer t) => {getOTP()});
    }
  }

  void getOTP() async {
    final messages = await _query
        .querySms(kinds: [SmsQueryKind.inbox], address: "027777777");

    for (final message in messages) {
      if (message.body.toString().contains("<OTP") &&
          message.body.toString().contains("<Ref.")) {
        //get OTP ref
        var ref = regExpRef.stringMatch(message.body.toString()).toString();
        var otp = regExpOtp.stringMatch(message.body.toString()).toString();
        if (ref.isNotEmpty && otp.isNotEmpty) {
          ref = ref.replaceAll("<Ref. ", "");
          ref = ref.replaceAll(">", "");
          otp = otp.replaceAll("<OTP ", "");
          otp = otp.replaceAll(">", "");
          final db = await database;
          //check old
          List<Map> result = await db.rawQuery(
              'SELECT * FROM otp WHERE ref=? AND otp=?', [ref, otp]);
          if(result.isEmpty){
            //send to API
            var data = <String, dynamic>{};
            data['token'] = '000000';
            data['ref'] = ref;
            data['otp'] = otp;
            data['message'] = message.body.toString();
            try {
              await http.post(
                Uri.parse("http://example.com/api/otp"),
                body: data,
              );
              await db.rawQuery(
                  'INSERT INTO otp (ref,otp,message) VALUES (?,?,?)',
                  [ref, otp, message.body.toString()]);
            }catch(e){
              if (kDebugMode) {
                print(e.toString());
              }
            }
          }
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container()),
    );
  }
}

class OTP {
  final int id;
  final String ref;
  final String otp;
  final String message;

  const OTP({
    required this.id,
    required this.ref,
    required this.otp,
    required this.message,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ref': ref,
      'otp': otp,
      'message': message,
    };
  }

  @override
  String toString() {
    return 'Otp{id: $id, ref: $ref, otp: $otp, message: $message}';
  }
}
